#include "server.h"
#include "ihello.h"

class SimpleHello : public Dom::Implement<SimpleHello, IHello> {
public:
	SimpleHello() { printf("%s\n", __PRETTY_FUNCTION__); }
	~SimpleHello() { printf("%s\n", __PRETTY_FUNCTION__); }
	virtual void Print() {
		printf("Hello from %s\n",__PRETTY_FUNCTION__);
	}
	define_clsuid(SimpleHello)
};

namespace Dom {

	DOM_SERVER_EXPORT_BEGIN
		EXPORT_CLASS(SimpleHello)
	DOM_SERVER_EXPORT_END

	DOM_SERVER_INSTALL(IUnknown* unknown) {
		Interface<IRegistryServer> registry;
		if (unknown->QueryInterface(IRegistryServer::guid(), registry)) {
		}
		return true;
	}

	DOM_SERVER_UNINSTALL(IUnknown* unknown) {
		Interface<IRegistryServer> registry;
		if (unknown->QueryInterface(IRegistryServer::guid(), registry)) {
		}
		return true;
	}
}