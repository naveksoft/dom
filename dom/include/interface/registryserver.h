#pragma once
#include "unknown.h"

namespace Dom {
	struct IRegistryServer : public virtual IUnknown {
		virtual bool RegisterInterface(const uiid) = 0;
		virtual bool UnRegisterInterface(const uiid) = 0;
		virtual bool InterfaceExist(const uiid) = 0;
		
		define_uiid(RegistryServer)
	};
}