#pragma once
#include <string>

#define dom_guid_pre_name	"guid-"
#define dom_cls_pre_name	"clsuid-"

#define define_uiid(name) \
	inline static const Dom::uiid guid() { const static std::string idn(dom_guid_pre_name #name); return (const Dom::uiid)idn.c_str(); }

#define define_clsuid(name) \
	inline static const Dom::uiid guid() { const static std::string idn(dom_cls_pre_name #name); return (const Dom::uiid)idn.c_str(); }

namespace Dom {
	using uiid = char*;
	using clsuid = char*;

	static inline std::string clsid(const std::string& name) { return std::string(dom_cls_pre_name) + name; }
	static inline std::string iid(const std::string& name) { return std::string(dom_guid_pre_name) + name; }

	struct IUnknown
	{
		virtual long AddRef() = 0;
		virtual long Release() = 0;
		virtual bool QueryInterface(const uiid, void **ppv) = 0;

		define_uiid(Unknown)
	};
}