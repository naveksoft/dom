#pragma once
#include "dom.h"
#include "interface/registryserver.h"
#include <atomic>
#include <functional>
#include <cstring>

namespace Dom {
	using namespace std;

	template<typename T, typename I = void>
	struct cast_interface {
		const uiid ciid;
		cast_interface(const uiid iid) : ciid(iid) {
		}
		inline bool cast(const uiid iid, T* t, void **ppv) const {
			if (strcmp(ciid, iid) == 0) {
				*ppv = static_cast<I*>((T*)t);
				t->AddRef();
				return true;
			}
			return false;
		}
	};

	/* Object server interfaces implement */
	template <typename T, typename ... IFACES>
	struct Embedded : virtual public IUnknown, virtual public IFACES... {
	protected:
		atomic<long> refs;
	public:
		Embedded() : refs(1) { ; }
		virtual ~Embedded() { ; }
		inline virtual long AddRef() { return refs; }
		inline virtual long Release() { return refs; }
		inline virtual bool QueryInterface(const uiid iid, void **ppv) {
			const cast_interface<T> guids[] = { IFACES::guid()...,IUnknown::guid() };
			*ppv = nullptr;
			for (auto&& it : guids) {
				if (it.cast(iid, (T*)this, ppv)) { return true; }
			}
			return false;
		}
		template<typename I>
		inline I* GetInterface() {
			AddRef();
			return static_cast<I*>((T*)this);
		}
	};

	/* Object server interfaces implement */
	template <typename T, typename ... IFACES>
	struct Implement : virtual public IUnknown, virtual public IFACES... {
	protected:
		atomic<long> refs;
	public:
		Implement() : refs(0) { ; }
		virtual ~Implement() { ; }
		inline virtual long AddRef() { extern atomic<long> DllRefs; DllRefs += 1; return ++refs; }
		inline virtual long Release() {
			extern atomic<long> DllRefs;
			DllRefs -= 1;
			if (--refs == 0) {
				delete static_cast<T*>(this);
				return 0;
			}
			return refs;
		}
		inline virtual bool QueryInterface(const uiid iid, void **ppv) {
			const cast_interface<T> guids[] = { IFACES::guid()...,IUnknown::guid() };
			*ppv = nullptr;
			for (auto&& it : guids) {
				if (it.cast(iid, (T*)this, ppv)) { return true; }
			}
			return false;
		}
		static bool SelfCreate(const uiid iid, void **ppv)
		{
			T *cls = new T;
			if (cls != nullptr) {
				if (cls->QueryInterface(iid, ppv)) {
					return true;
				}
				delete cls;
			}
			return false;
		}

	};
}

#define DOM_SERVER_INITIALIZE extern "C" bool DllInitialize
#define DOM_SERVER_FINALIZE extern "C" bool DllFinalize

#define DOM_SERVER_INSTALL extern "C" bool DllInstall

#define DOM_SERVER_UNINSTALL extern "C" bool DllUnInstall

#define DOM_SERVER_EXPORT_BEGIN \
	std::atomic<long>	DllRefs(0);\
	typedef bool (*create_new_object)(const uiid iid,void** ppv);\
	static const struct { const string iid; create_new_object create; } class_exports_table[] = {

#define DOM_SERVER_EXPORT_END \
	};\
	extern "C" {\
	bool DllCreateInstance(const uiid iid, void** ppv) {\
	*ppv = nullptr;\
	for(auto&& it : class_exports_table) {\
		if (strcmp(it.iid.c_str(), iid) == 0) \
			return it.create(IUnknown::guid(), ppv);\
	}\
	return false; }\
	bool DllCanUnloadNow() { return DllRefs == 0; } \
	bool DllRegisterServer(IUnknown* unknown) { \
		Interface<IRegistryServer> registry;\
		if(unknown->QueryInterface(IRegistryServer::guid(),registry)) {\
			for(auto&& it : class_exports_table) {\
				if(!registry->RegisterInterface((const Dom::uiid)it.iid.c_str())) return false;\
			}\
			return true;\
		}\
		return false; \
	} \
	bool DllUnRegisterServer(IUnknown* unknown) { \
		Interface<IRegistryServer> registry;\
		if(unknown->QueryInterface(IRegistryServer::guid(),registry)) {\
			for(auto&& it : class_exports_table) {\
				if(!registry->UnRegisterInterface((const Dom::uiid)it.iid.c_str())) return false;\
			}\
			return true;\
		}\
		return false; \
	}\
}

#define EXPORT_CLASS(CLASS) { CLASS::guid(),reinterpret_cast<create_new_object>(Implement<CLASS>::SelfCreate) },
