#pragma once
#include "interface/unknown.h"
#include <vector>

namespace Dom /* Dynamic object model */ {
	using namespace std;
	/* Object client interfaces implement */
	template <class T>
	class Interface
	{
	private:
		T * _i;
	public:
		Interface() : _i(nullptr) { ; }
		Interface(T* lp) : _i(lp) { _i != nullptr && _i->AddRef(); }
		Interface(const Interface<T>&) = delete;
		Interface<T>& operator = (const Interface<T>&) = delete;
		Interface(const Interface<T>&& lp) : _i(lp._i) { _i != nullptr && _i->AddRef(); }
		Interface<T>& operator = (const Interface<T>&& lp)
		{
			Release();
			_i = lp._i;
			_i != nullptr && _i->AddRef();
			return *this;
		}
		~Interface() throw() { Release(); }
		operator void** () { return (void**)&_i; }
		operator T*() const { return _i; }
		T& operator*() const { return *_i; }
		T** operator&() { return &_i; }
		T* operator->() const { return _i; }
		bool operator!() const { return (_i == nullptr); }
		operator bool() const { return (_i != nullptr); }
		bool operator==(T* pT) const { return _i == pT; }

		void Release() { T* pTemp = _i; if (pTemp != nullptr) { _i = nullptr; pTemp->Release(); } }

		template <typename I>
		bool QueryInterface(const uiid &objid, I **ppIRequired) { return _i->QueryInterface(objid, (void **)ppIRequired); }

		bool QueryInterface(IUnknown* unknown) { 
			Release(); 
			return (unknown != nullptr ? unknown->QueryInterface(T::guid(), (void **)&_i) : false);
		}

		static const uiid guid() { return T::guid(); }
	};

	bool CreateInstance(const Dom::clsuid objid, void **ppIRequired, std::string scope = std::string());
	bool RegisterServer(std::string sopathame, std::string scope = std::string());
	bool UnRegisterServer(std::string sopathame, std::string scope = std::string());
	std::vector<std::pair<std::string, std::string>> EnumObjects(std::string scope = std::string());
}