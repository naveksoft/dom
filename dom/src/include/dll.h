#pragma once
#include <string>
#include "../../include/server.h"
#include <sys/stat.h>
#include <unistd.h>

namespace Dom {
	extern "C"
	{
		typedef bool(*__DllCreateInstance)(const uiid&, void**);
		typedef bool(*__DllCanUnloadNow)();
		typedef bool(*__DllRegisterServer)(IUnknown* registry);
		typedef bool(*__DllUnRegisterServer)(IUnknown* registry);
		typedef bool(*__DllInstall)(IUnknown* registry);
		typedef bool(*__DllUnInstall)(IUnknown* registry);
	}

	class Dll {
	private:
		void*					_handle;
		string					_soname;
		__DllCreateInstance		_createinstance;
		__DllCanUnloadNow		_canunloadnow;
		__DllRegisterServer		_registerserver;
		__DllUnRegisterServer	_unregisterserver;
		__DllInstall			_install;
		__DllUnInstall			_uninstall;

		inline void Open() throw (string) {
			if (_handle == nullptr && !_soname.empty()) {
				_handle = dlopen(_soname.c_str(), RTLD_NOW);
				if (_handle == nullptr) {
					throw string(dlerror());
				}
				_createinstance = (__DllCreateInstance)dlsym(_handle, "DllCreateInstance");
				_canunloadnow = (__DllCanUnloadNow)dlsym(_handle, "DllCanUnloadNow");
				_registerserver = (__DllRegisterServer)dlsym(_handle, "DllRegisterServer");
				_unregisterserver = (__DllUnRegisterServer)dlsym(_handle, "DllUnRegisterServer");
				_install = (__DllInstall)dlsym(_handle, "DllInstall");
				_uninstall = (__DllUnInstall)dlsym(_handle, "DllUnInstall");
				if (_createinstance == nullptr || _canunloadnow == nullptr || _registerserver == nullptr || _unregisterserver == nullptr) {
					Close();
					throw string("Invalid DOM objects server.");
				}
			}
		}

		inline void Close() throw (string) {
			if (_handle != nullptr) {
				(*_canunloadnow)() && dlclose(_handle);
				_handle = nullptr;
				_createinstance = nullptr;
				_canunloadnow = nullptr;
				_registerserver = nullptr;
				_unregisterserver = nullptr;
				_install = nullptr;
				_uninstall = nullptr;
			}
		}
	public:
		Dll(string so) :
			_handle(nullptr), _soname(so), _createinstance(nullptr), _canunloadnow(nullptr), 
			_registerserver(nullptr), _unregisterserver(nullptr), _install(nullptr), _uninstall(nullptr) {
			Open();
		}

		Dll(const Dll&& so) :
			_handle(so._handle), _soname(so._soname), _createinstance(so._createinstance), _canunloadnow(so._canunloadnow), 
			_registerserver(so._registerserver), _unregisterserver(so._unregisterserver), _install(so._install), _uninstall(so._uninstall) {
			;
		}

		~Dll() { Close(); }

		inline Dll& operator = (const Dll&& so) {
			_handle = so._handle;
			_soname = move(so._soname);
			_createinstance = so._createinstance;
			_canunloadnow = so._canunloadnow;
			_registerserver = so._registerserver;
			_unregisterserver = so._unregisterserver;
			_install = so._install;
			_uninstall = so._uninstall;
			return *this;
		}

		template <typename I>
		inline bool CreateInstance(const uiid &objid, I **ppIRequired) throw (string) { return _createinstance != nullptr ? (*_createinstance)(objid, (void **)ppIRequired) : false; }

		inline bool RegisterServer(IUnknown* registry) {
			return (_registerserver != nullptr) ? (*_registerserver)(registry) : false;
		}

		inline bool UnRegisterServer(IUnknown* registry) {
			return (_unregisterserver != nullptr) ? (*_unregisterserver)(registry) : false;
		}

		inline bool Install(IUnknown* registry) {
			return (_install != nullptr) ? (*_install)(registry) : true;
		}

		inline bool UnInstall(IUnknown* registry) {
			return (_uninstall != nullptr) ? (*_uninstall)(registry) : true;
		}
	};
}