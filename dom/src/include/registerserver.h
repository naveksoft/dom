#pragma once
#include "../../include/server.h"
#include "../../include/interface/registryserver.h"
#include <functional>
#include <dirent.h>


namespace Dom {
	using namespace std;

	template<size_t SZ, typename T = char>
	class static_buffer {
	private:
		T	buffer[SZ + 1];
	public:
		inline T * operator &() { return buffer; }
		inline size_t size() { return SZ; }
		inline T& operator [](size_t i) { return buffer[i]; }

		operator const char* () { return (const char*)buffer; }
	};

	class CRegistryServer : public Embedded<CRegistryServer, IRegistryServer> {
	public:
		string so_pathname, so_scope;
		bool	so_silent;

		static void enum_files(const string &path, std::function<void(const string&, const struct ::dirent &)>&& cb, bool recursive = true) {

			if (auto dir = opendir(path.c_str())) {
				while (auto f = readdir(dir)) {
					if (!f->d_name || f->d_name[0] == '.') continue;

					if (f->d_type & DT_DIR) {
						cb(path + f->d_name, *f);
						if (recursive) enum_files(path + (*path.end() == '/' ? "" : "/") + f->d_name + "/", move(cb), recursive);
					}
					else if (f->d_type & DT_REG)
						cb(path + (*path.end() == '/' ? "" : "/") + f->d_name, *f);
				}
				closedir(dir);
			}
			else if (errno > 0) {
				printf("[ERROR] %s\n", strerror(errno));
			}
		}


		static bool dir_exist(const std::string& path)
		{
			struct stat info;
			if (stat(path.c_str(), &info) != 0)
			{
				return false;
			}
			return (info.st_mode & S_IFDIR) != 0;
		}

		static int makedir(std::string dirname, int mode)
		{
			if (mkdir(dirname.c_str(), mode) == 0)
				return true;
			switch (errno)
			{
			case ENOENT:
			{
				size_t pos = dirname.find_last_of('/');
				if (pos == std::string::npos)
					return false;
				if (!makedir(dirname.substr(0, pos), mode))
					return false;
				return 0 == mkdir(dirname.c_str(), mode);
			}
			case EEXIST:
			{
				return dir_exist(dirname);
			}
			}
			return false;
		}

		static int register_so(string iid, string so, string& scope) {
			makedir(scope, 0660);
			iid.insert(0, "/").insert(0, scope);
			return symlink(so.c_str(), iid.c_str()) == 0 ? 0 : errno;
		}

		static int unregister_so(string iid, string& scope) {
			iid.insert(0, "/").insert(0, scope);
			return remove(iid.c_str()) == 0 ? 0 : errno;
		}

		static string target_so(string iid, string& scope) {
			iid.insert(0, "/").insert(0, scope);
			return target_so(iid);
		}

		static string target_so(const string& so_path) {
			static_buffer<512> buf; buf[0] = '\0';

			int count = (int)readlink(so_path.c_str(), &buf, (int)buf.size());
			if (count >= 0) {
				buf[count] = '\0';
			}
			return (const char*)buf;
		}
	public:
		CRegistryServer(string so, string scope,bool silent = false) : so_pathname(so), so_scope(scope), so_silent(silent){ ; }

		virtual bool RegisterInterface(const uiid& iid) {
			switch (auto result = register_so(iid.c_str(), so_pathname, so_scope)) {
			case 0:
				!so_silent && printf("[ DOM ] %s -> %s ( SUCCESS. REGISTER )\n", iid.c_str(), so_scope.c_str());
				return true;
			case EEXIST:
				!so_silent && printf("[ DOM ] %s -> %s ( WARNING. ALREADY REGISTERED)\n", iid.c_str(), so_scope.c_str());
				return true;
			default:
				!so_silent && printf("[ DOM ] %s -> %s ( REGISTER ERROR. %s)\n", iid.c_str(), so_scope.c_str(),strerror(result));
			}
			return false;
		}
		virtual bool UnRegisterInterface(const uiid& iid) {
			switch (auto result = unregister_so(iid.c_str(), so_scope)) {
			case 0:
				!so_silent && printf("[ DOM ] %s -> %s ( SUCCESS. UNREGISTER )\n", iid.c_str(), so_scope.c_str());
				return true;
			case ENOENT:
				!so_silent && printf("[ DOM ] %s -> %s ( WARNING. NOT REGISTERED )\n", iid.c_str(), so_scope.c_str());
				return true;
			default:
				!so_silent && printf("[ DOM ] %s -> %s ( UNREGISTER ERROR. %s )\n", iid.c_str(), so_scope.c_str(), strerror(result));
			}
			return false;
		}

		virtual bool InterfaceExist(const uiid& iid) {
			string iface(iid);
			iface.insert(0, "/").insert(0, so_scope);
			struct stat info;
			return (stat(iface.c_str(), &info) != 0) && ((info.st_mode & S_IFLNK) != 0);
		}

		inline bool RegisterServer() {
			auto so = Dll(so_pathname);
			if (so.RegisterServer(GetInterface<IUnknown>())) {
				if (!so.Install(GetInterface<IUnknown>())) {
					so.UnRegisterServer(GetInterface<IUnknown>());
					!so_silent && printf("[ DOM ] %s ( INSTALL ERROR. Can not be registered properly )\n", so_pathname.c_str());
				}
				!so_silent && printf("[ DOM ] %s ( SUCCESS. REGISTER )\n", so_pathname.c_str());
				return true;
			}
			!so_silent && printf("[ DOM ] %s ( REGISTER ERROR. Invalid dom library )\n", so_pathname.c_str());
			return false;
		}

		bool UnRegisterServer() {
			auto so = Dll(so_pathname);

			if (!so.UnInstall(GetInterface<IUnknown>())) {
				!so_silent && printf("[ DOM ] %s ( UNINSTALL WARNING. Can not be uninstalling properly )\n", so_pathname.c_str());
			}

			if (so.UnRegisterServer(GetInterface<IUnknown>())) {
				!so_silent && printf("[ DOM ] %s ( SUCCESS. UNREGISTER )\n", so_pathname.c_str());
				return true;
			}

			!so_silent && printf("[ DOM ] %s ( UNREGISTER ERROR. Invalid dom library )\n", so_pathname.c_str());

			return false;
		}

		vector<pair<string,string>> Enum(bool subscopes) {
			vector<pair<string,string>> list;

			enum_files(so_scope, [&list](const string& path, const struct ::dirent & entry)->void {
				if ((entry.d_type & DT_REG) && strncmp(entry.d_name, dom_cls_pre_name, sizeof(dom_cls_pre_name) - 1) == 0) {
					auto target = target_so(path);
					auto pos = target.find_last_of('/');
					if (pos != string::npos) {
						target = target.substr(pos + 1);
					}
					list.emplace_back(entry.d_name + (sizeof(dom_cls_pre_name) - 1), target);
				}
			}, subscopes);

			return list;
		}
	};
}