#ifdef	DOM_REGSRV
#include <cstdio>
#include "../include/dom.h"

#include "include/cmdline.h"

void print_option(string loption, string soption, string args, const char* help) {
	string opt = loption + " " + args;
	if (soption.empty()) {
		printf("%3s %-40s", " ", opt.c_str());
	}
	else {
		printf("%2s, %-40s", soption.c_str(), opt.c_str());
	}
	bool first = true;
	for (auto&& h : explode("\n", help)) {
		if (first) {
			first = false;
			printf("%s\n", h.c_str());
		}
		else {
			printf("%40s%s\n", " ", h.c_str());
		}
	}
}

void print_help() {
	printf(
		"Register\\Unregister Dynamic Object Model component server.\n"\
		"Usage: regsrv [options] shared_object.so\n");
	print_option("--scope", "-s", "<scope path>", "path to register dom component\n\tdefault: /opt/dom/");
	print_option("--unregister", "-u", "", "unregister component (default)");
	print_option("--list", "-l", "", "list registered dom components");
	print_option("--help", "-h", "", "print this help");
	exit(0);
}

int main(int argc,char* argv[])
{
	command_line cmdline;
	/* Define command line argumnets, long option name are required */

	try {
		cmdline.options(argc, argv,
			command_line::option("scope", 1, 's'),
			command_line::option("unregister", 0, 'u'),
			command_line::option("list", 0, 'l'),
			command_line::option("help", 0, 'h')
		);
		string scope = cmdline("scope", "");
		if (argc == 1 || cmdline.is("--help")) {
			print_help();
		}
		else if (cmdline.is("unregister")) {
			Dom::UnRegisterServer(argv[argc - 1], scope);
		}
		else if (cmdline.is("list")) {
			string norm_scope(scope), cur_so;
			if (*(norm_scope.end()) != '/') {
				norm_scope += '/';
			}
			for (auto&& com : Dom::EnumObjects(scope)) {
				if (cur_so != com.second) {
					printf("%s%s:\n", norm_scope.c_str(), com.second.c_str());
					cur_so = com.second;
				}
				printf("    -> %s\n", com.first.c_str());
			}
		}
		else {
			Dom::RegisterServer(argv[argc - 1], scope);
		}
	}
	catch (string ex) {

	}
	return 0;
}

#endif

